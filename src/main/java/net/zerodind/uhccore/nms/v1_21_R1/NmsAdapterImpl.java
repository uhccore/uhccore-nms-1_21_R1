package net.zerodind.uhccore.nms.v1_21_R1;

import org.bukkit.scoreboard.Score;
import org.bukkit.craftbukkit.scoreboard.CraftScoreboard;

import net.minecraft.core.registries.Registries;
import net.minecraft.world.level.levelgen.DensityFunction;
import net.minecraft.world.level.levelgen.DensityFunctions;
import net.minecraft.world.level.levelgen.NoiseRouterData;
import net.minecraft.world.scores.Objective;
import net.minecraft.world.scores.ScoreAccess;
import net.minecraft.world.scores.ScoreHolder;
import net.minecraft.world.scores.Scoreboard;
import net.zerodind.uhccore.nms.NmsAdapter;
import net.zerodind.uhccore.nms.NmsOperationException;

public class NmsAdapterImpl implements NmsAdapter {

	@Override
	public void removeOceans() throws NmsOperationException {
		RegistryUtils.overrideHolder(Registries.DENSITY_FUNCTION, NoiseRouterData.CONTINENTS, continents -> {
			// The original continents DF is wrapped in a FlatCache.
			// See net.minecraft.world.level.levelgen.NoiseRouterData.bootstrap()
			final DensityFunction continentsUnwrapped;
			try {
				continentsUnwrapped = ((DensityFunctions.MarkerOrMarked) continents).wrapped();
			} catch (ClassCastException e) {
				throw new NmsOperationException(e);
			}

			// We keep the FlatCache for performance, but take the absolute value of the noise function.
			// This stops ocean generation, but keeps the qualities of the original noise.
			return DensityFunctions.flatCache(continentsUnwrapped.abs());
		});
	}

	@Override
	public void setReadOnlyPlayerScore(Score playerScore, int value) {
		final Scoreboard nmsScoreboard = ((CraftScoreboard) playerScore.getScoreboard()).getHandle();
		final ScoreHolder nmsScoreHolder = ScoreHolder.forNameOnly(playerScore.getEntry());
		final Objective nmsObjective = nmsScoreboard.getObjective(playerScore.getObjective().getName());
		// forceWritable==true is used to update the read-only score
		final ScoreAccess nmsScoreAccess = nmsScoreboard.getOrCreatePlayerScore(nmsScoreHolder, nmsObjective, true);
		nmsScoreAccess.set(value);
	}

}
